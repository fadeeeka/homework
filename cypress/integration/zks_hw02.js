describe('ToDo list items', function () {
    before(() => {
        cy.visit('/')
        cy.wait(200)
        cy.get('.todo-list li').should('have.length', 0)
        cy.screenshot()
    })

  beforeEach(() => {
        cy.wait(1000)
    })

    afterEach(() =>{
        cy.screenshot()
        cy.wait(500)
    })

    it('Enter Items to ToDo list', function () {


             cy.get('.new-todo')
            .type(`${'drink coffee'} {enter}`)
            .type(`${'watching TV'} {enter}`)
            .type(`${'buy HappyMeal'} {enter}`)
            .type(`${'clean smartphone'} {enter}`)
    })
    it('Select 1th element as completed', function () {

        cy.contains('.todo-list li', 'drink coffee')
            .find('.toggle').check()
        cy.contains('.todo-list li', 'drink coffee')
            .should('have.class', 'completed')
    })

    it('change some css properties and complete 4rd item', function () {

        cy.contains('.todo-list li', 'clean smartphone')
            .find('.toggle').check()

        cy.get('body')
            .invoke('css', 'background-color', 'blue')
            .should('have.css', 'background-color', 'rgb(0, 0, 255)')

        cy.get('h1')
            .invoke('css', 'color', 'rgb(128, 0, 0)')
            .should('have.css', 'color', 'rgb(128, 0, 0)')

        cy.get('.todoapp')
            .invoke('css', 'background-color', 'rgb(128, 0, 128)')
            .should('have.css', 'background-color', 'rgb(128, 0, 128)')

        cy.get('.todoapp')
            .invoke('css', 'font-family', 'Verdana')
            .should('have.css', 'font-family', 'Verdana')
            
        cy.screenshot()
        cy.wait(500)
        cy.get('.todo-list').toMatchImageSnapshot({
                    threshold: 0.001,
                })
    })
})
